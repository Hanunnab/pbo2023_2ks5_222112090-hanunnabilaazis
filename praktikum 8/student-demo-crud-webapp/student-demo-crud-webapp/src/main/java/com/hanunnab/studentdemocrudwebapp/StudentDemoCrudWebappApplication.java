package com.hanunnab.studentdemocrudwebapp;

/*merupakan sebuah kelas dengan entry point
menggunakan auto-configuration spring framework*/

import org.springframework.boot.SpringApplication; //untuk memulai spingboot
import org.springframework.boot.autoconfigure.SpringBootApplication; //auto konfigurasi


//entry point springboot project
@SpringBootApplication
public class StudentDemoCrudWebappApplication {

	public static void main(String[] args) { 
        //method main memanggil method run dari class SpringApplication dengan parameter studentdemo...
		SpringApplication.run(StudentDemoCrudWebappApplication.class, args);
	}

}
