/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 *
 * Author = 222112090_Hanun Nabila Azis
 * Deskripsi singkat kelas
 * kelas Mahasiswa3 dan menginisialisasi nilai ke objek dengan memanggil method tambahData
 * 
 */
public class Mahasiswa3 {
    int nim; //inisialisasi atribut
    String nama;
    
    void tambahData (int vnim, String vnama) {
        nim = vnim;
        nama = vnama;
    }
    
    void tampilkanInfo() { //tampil data
        System.out.println(nim + " " + nama);
    }
    
}
