/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul4;

import java.util.Date;

/**
 *
 * @author hanunnab
 */
abstract class Orang { //membuat kelas abstract orang
    String nama;
    Date tanggalLahir;
    
    public Orang () {
    }
    
    public Orang (String nama) {
        this.nama = nama;
    }
    
    public Orang (String nama, Date tanggalLahir) {
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }
    
    public void setNama (String nama) {
        this.nama = nama;
    }
    
    public String getNama () {
        return nama;
    }
    
    public Date getTanggalLahir () {
        return tanggalLahir;
    }
    
    public void setTanggalLahir (Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
    
    public String getGaji () {
        return "tidak ada";
    }
    
    //method abstract dan harus dimiliki disetiap kelas turunannya
    abstract public String getAlamat();
    abstract public void setAlamat(String alamat);
}

