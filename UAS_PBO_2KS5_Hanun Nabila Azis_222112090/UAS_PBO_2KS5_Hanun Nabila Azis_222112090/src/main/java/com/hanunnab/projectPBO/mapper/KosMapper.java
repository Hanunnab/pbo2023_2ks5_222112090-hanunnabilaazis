/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.projectPBO.mapper;

import com.hanunnab.projectPBO.dto.KosDto;
import com.hanunnab.projectPBO.entity.Kos;

/**
 *
 * @author asus
 */
public class KosMapper {
    //map Student entity to Student Dto
    public static KosDto mapToKosDto(Kos kos) {
        //membuat dto dengan builder pattern (inject dari lombok)
        KosDto kosDto = KosDto.builder()
            .id (kos.getId())
            .namaKos(kos.getNamaKos())
            .fasilitas(kos.getFasilitas())
            .harga(kos.getHarga())
            .daerah(kos.getDaerah())
            .kontak(kos.getKontak())
            .build();
        
return kosDto;
    }
    public static Kos mapToKos (KosDto kosDto) {
    Kos kos = Kos.builder()
        .id (kosDto.getId())
        .namaKos(kosDto.getNamaKos())
        .fasilitas(kosDto.getFasilitas())
        .harga(kosDto.getHarga())
        .daerah(kosDto.getDaerah())
        .kontak(kosDto.getKontak())
        .build();
    return kos;
    }
    
}
