/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.projectPBO.dto;

import com.hanunnab.projectPBO.entity.Kos;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author asus
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DaerahDto {
    private Long id;
    @NotEmpty (message = "Isi nama daerah")
    private String namaDaerah;
    
  //@OneToMany (cascade = CascadeType.ALL, fetch = FetchType.LAZY)
  //  @JoinColumn (name = "daerah_id")
    private List<Kos> kos = new ArrayList<>();
     
   @Override
    public String toString() {
        return namaDaerah;
    }
}
