/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.hanunnab.projectPBO.service;
import java.util.List;
import com.hanunnab.projectPBO.dto.UserDto;
import com.hanunnab.projectPBO.entity.User;

/**
 *
 * @author asus
 */
public interface UserService {
    void saveUser(UserDto userDto);

    User findUserByEmail(String email);

    List<UserDto> findAllUsers();
}
