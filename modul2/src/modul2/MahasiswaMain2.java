/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 *
 * Author = 222112090_Hanun Nabila Azis
 * Deskripsi singkat kelas
 * kelas MahasiswaMain2 yang menerapkan fungsi variabel referensi
 */
public class MahasiswaMain2 { //Membuat kelas MahasiswaMain2
    public static void main(String args[]) { 
        Mahasiswa2 s1 = new Mahasiswa2(); //membuat objek
        //inisiasi atribut
        s1.nim = 222112090;
        s1.nama = "Hanun";
        System.out.println(s1.nim); //memanggil variabel referensi dari kelas Mahasiswa2 dengan variabel diisi di atas
        System.out.println(s1.nama);
    }
    
}
