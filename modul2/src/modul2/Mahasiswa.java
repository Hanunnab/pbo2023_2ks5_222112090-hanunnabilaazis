/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 *
 * Author = 222112090_Hanun Nabila Azis
 * 
 */
public class Mahasiswa { //nama kelas : Mahasiswa
    //mendefinisikan atribut
    int nim;
    String nama;
    
    //membuat main method dalam kelas Mahasiswa
    public static void main (String args[]){
        Mahasiswa s1=new Mahasiswa(); //membuat objek mahasiswa
                //mencetak nilai objek
                System.out.println(s1.nim); //mengakses melalui variabel referensi
                System.out.println(s1.nama);
    }
    
}
