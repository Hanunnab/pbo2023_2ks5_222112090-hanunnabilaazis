/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 *
 * Author = 222112090_Hanun Nabila Azis
 * Deskripsi singkat kelas
 * mencetak dan menyimpan informasi dalam variabel referensi
 */
public class MahasiswaMain3 { //membuat kelas MahasiswaMain3
    public static void main(String args[]) {//inisiasi main method
        Mahasiswa2 s1 = new Mahasiswa2();
        Mahasiswa2 s2 = new Mahasiswa2();
        //inisiasi atribut
        s1.nim = 222112090;
        s1.nama = "Hanun";
        s2.nim = 222112100;
        s2.nama = "Nabila";
        System.out.println(s1.nim+" "+s1.nama); //menampilkan output
        System.out.println(s2.nim+" "+s2.nama);
    }
}
