/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.projectPBO.controller;

import java.util.List;
import com.hanunnab.projectPBO.service.DaerahService;
import com.hanunnab.projectPBO.repository.DaerahRepository;
import org.springframework.stereotype.Service;
import com.hanunnab.projectPBO.entity.Daerah;
import com.hanunnab.projectPBO.dto.DaerahDto;
import com.hanunnab.projectPBO.entity.Kos;
import com.hanunnab.projectPBO.mapper.DaerahMapper;
import com.hanunnab.projectPBO.repository.KosRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author asus
 */
@Controller
public class DaerahController {
    private DaerahService daerahService;
    public DaerahController (DaerahService daerahService) {
        this.daerahService = daerahService;
    }
    @Autowired
    private KosRepository kosRepo;
    
    @GetMapping("/admin/daerahs")
    public String daerahs (Model model) {
        List<DaerahDto> daerahDtos = this.daerahService.ambilDaerah();
        model.addAttribute("daerahDtos", daerahDtos);
        return "/admin/daerahs";
    }
    
    @GetMapping ("/admin/daerahs/add")
    public String addDaerahForm(Model model){
        DaerahDto daerahDto = new DaerahDto();
//        List<Kos> listKoss = kosRepo.findAll();
//        model.addAttribute("listKoss", listKoss);
        // tambah atribut "studentDto" yang bisa/akan digunakan di form th:object
        model.addAttribute("daerahDto",daerahDto );
        // thymeleaf view: "/templates/admin/students.html"
        return "/admin/daerah_add_form";
    }
    
//    @GetMapping("/")
//    public String index (){
//        return "index";
//    }
    
    @PostMapping("/admin/daerahs/add")
    public String addDaerah(@Valid DaerahDto daerahDto, BindingResult result,Model model){
        if (result.hasErrors()){
//            List<Kos> listKoss = kosRepo.findAll();
//            model.addAttribute("listKoss", listKoss);
            // model.addAttribute("studentDto", studentDto);
            return "/admin/daerah_add_form";
        }
        daerahService.simpanDataDaerah(daerahDto);
        return "redirect:/admin/daerahs";
    }
    
    @GetMapping("/admin/daerahs/delete/{id}")
    public String deleteDaerah(@PathVariable("id") Long id){
        daerahService.hapusDataDaerah(id); 
        return "redirect:/admin/daerahs";
    }
    
    @GetMapping("/admin/daerahs/update/{id}")
    public String updateDaerahForm(@PathVariable("id") Long id, Model model){
        DaerahDto daerahDto = daerahService.cariById(id); 
//        List<Kos> listKoss = kosRepo.findAll();
//        model.addAttribute("listKoss", listKoss);
        model.addAttribute("daerahDto", daerahDto);
        return "/admin/daerah_update_form";
    }

    @PostMapping("/admin/daerahs/update")
    public String updateDaerah(@Valid @ModelAttribute("daerahs") DaerahDto daerahDto, BindingResult result){
        if (result.hasErrors()){
            // model.addAttribute("studentDto", studentDto);
            return "/admin/daerah_update_form";
        }
        daerahService.perbaruiDataDaerah(daerahDto);
        return "redirect:/admin/daerahs";
    }
}
