/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 *
 * Author = 222112090_Hanun Nabila Azis
 * Deskripsi singkat kelas
 * kelas Mahasiswa4 yang menerapkan inisialisasi melalui konstruktor
 * 
 */

public class Mahasiswa4 { //inisialiasasi kelas
    int nim; //inisialisasi atribut
    String nama;
    
    Mahasiswa4(int vnim, String vnama) {
    nim = vnim;
    nama = vnama;
    }    
    
    void tampilkanInfo (){
        System.out.println (nim + " " + nama);
    }
}
