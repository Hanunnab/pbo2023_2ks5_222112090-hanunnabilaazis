/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul4;

import java.util.Date;

/**
 *
 * @author hanunnab
 */

//implementasi interface Dosen di Pegawai
public class Pegawai extends Orang implements Dosen {
    private String NIP;
    private String namaKantor;
    private String unitKerja;
    private String alamat; //menambahkan atribut alamat
    private String NIDN; //menambahkan atribut sesuai dengan interface Dosen
    private String keahlian; //menambahkan atribut sesuai dengan interface Dosen
    
    public Pegawai () {
    }
    
    public Pegawai(String NIP, String namaKantor, String unitKerja){
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    public Pegawai (String nama, String NIP, String namaKantor, String unitKerja) {
        super(nama);
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    public Pegawai(String nama, Date tanggalLahir, String NIP, String namaKantor, String unitKerja){
        super(nama, tanggalLahir);
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    public String getNIP () {
        return NIP;
    }
    
    public void setNIP (String NIP) {
        this.NIP = NIP;
    } 
    
    public String getNamaKantor () {
        return namaKantor;
    }
    
    public void setNamaKantor (String namaKantor) {
        this.namaKantor = namaKantor;
    }
    
    public String getUnitKerja () {
        return unitKerja;
    }
    
    public void setUnitKerja (String unitKerja) {
        this.unitKerja = unitKerja;
    } 
    
    @Override
    public String getGaji () {
        return "7 Juta";
    }
    
    @Override
    public String getAlamat() { //metode getter untuk mendapatkan dan mengembalikan alamat
        return alamat;
    };
    
    @Override
    public void setAlamat (String alamat) { //metode setter untuk mengeset dan mengubah alamat
        this.alamat=alamat;
    }
    
    //implementasi method di interface Dosen
    @Override
    public String getNIDN() {
        return NIDN;
    }
    
    //implementasi method di interface Dosen
    @Override
    public void setNIDN(String NIDN) {
        this.NIDN = NIDN;
    }
    
    //implementasi method di interface Dosen
    @Override
    public String getKelompokKeahlian() {
        return keahlian;
    }
    
    //implementasi method di interface Dosen
    @Override
    public void setKelompokKeahlian(String kelompokKeahlian) {
        this.keahlian=kelompokKeahlian;
    }
}
