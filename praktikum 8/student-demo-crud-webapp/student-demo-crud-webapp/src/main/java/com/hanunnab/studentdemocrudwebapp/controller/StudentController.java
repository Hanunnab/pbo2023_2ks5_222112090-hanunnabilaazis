/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.studentdemocrudwebapp.controller;

/*menangani permintaan user terhadap data student yang tersimpan di database*/

import com.hanunnab.studentdemocrudwebapp.dto.StudentDto;
import com.hanunnab.studentdemocrudwebapp.service.StudentService;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author asus
 */
@Controller
public class StudentController {
    private StudentService studentService;
    public StudentController(StudentService studentService){
        this.studentService = studentService;
    }
    
    @GetMapping("/admin/students")
    public String students(Model model){
        List<StudentDto> studentDtos = this.studentService.ambilDaftarStudent();
        model.addAttribute("studentDtos",studentDtos);
        return "/admin/students";
    }
   
    @GetMapping("/")
    public String index(){
        return "index";
    }
    @GetMapping("admin/students/add")
    public String addStudentForm(Model model){
        StudentDto studentDto = new StudentDto();
        model.addAttribute("studentDto",studentDto);
        return "/admin/student_add_form";
    }
    @PostMapping("/admin/students/add")
    public String addStudent(@Valid StudentDto studentDto, BindingResult result){
        if (result.hasErrors()){
            return "/admin/student_add_form";
        }
        studentService.simpanDataStudent(studentDto);
        return "redirect:/admin/students";
    }
    @GetMapping("/admin/students/delete/{id}")
    public String deleteStudent(@PathVariable("id") Long id){
        studentService.hapusDataStudent(id);
        return "redirect:/admin/students";
    }
    @GetMapping("/admin/students/update/{id}")
    public String updateStudentForm(@PathVariable("id") Long id, Model model){
        StudentDto studentDto = studentService.cariById(id);
        model.addAttribute("studentDto", studentDto);
        return "/admin/student_update_form";
    }
    @PostMapping("/admin/students/update")
    public String updateStudent(@Valid @ModelAttribute("studentDto") StudentDto studentDto, BindingResult result){
        if (result.hasErrors()){
            return "/admin/student_update_form";
        }
        studentService.perbaruiDataStudent(studentDto);
        return "redirect:/admin/students";
    }
}