/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * Author : Hanun Nabila Azis
 */
public class Gedung {
    private List<Ruang> daftarRuang = new ArrayList<Ruang>();
    
    public Gedung () {
        Ruang ruang = new Ruang("Utama");
        daftarRuang.add(ruang);
    }
    
    public void addRuang (String namaRuang){
    Ruang ruang = new Ruang (namaRuang);
    daftarRuang.add (ruang);
    }
    
    public List<Ruang> getDaftarRuang () {
        return daftarRuang;
    }
    
    
}
