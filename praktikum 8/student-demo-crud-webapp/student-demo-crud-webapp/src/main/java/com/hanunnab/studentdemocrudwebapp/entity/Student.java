/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.studentdemocrudwebapp.entity;

/*class mendefinisikan entity Student yang akan digunakan pada aplikasi CRUD 
 web dengan framework Spring.*/

import jakarta.persistence.*;
//hibernate
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import java.util.Date;
import lombok.Setter;
import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity //class merepresentasikan entitas pada basis data
@Table(name="tbl_student") //menentukan nama tabel di basis data yang digunakan

/**
 *
 * @author asus
 */
public class Student {
    @Id //field adalah PK dalam tabel
    @GeneratedValue(strategy=GenerationType.IDENTITY) //meng-generate nilai id
    private Long id;
    @Column(nullable=false) //merepresentasikan kolom pada tabel di basis data
    private String firstName;
    @Column(nullable=false)
    private String lastName;
    @Column(nullable=false)
    @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
    private Date birthDate;
    @Column(nullable=false)
    @CreationTimestamp
    @DateTimeFormat (iso=DateTimeFormat.ISO.DATE)
    private Date createdOn;
    @UpdateTimestamp
    @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)
    private Date updatedOn;
}
