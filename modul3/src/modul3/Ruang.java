/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;

/**
 *
 *Author : Hanun Nabila Azis
 */
public class Ruang {
    private String namaRuang;
    
    public Ruang (String namaRuang) {
        this.namaRuang = namaRuang;
    }
    
    public String getNamaRuang () {
        return namaRuang;
    }
    
    public void setNamaRuang (String namaRuang) {
        this.namaRuang = namaRuang;
    }
    
}
