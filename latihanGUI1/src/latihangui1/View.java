/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package latihangui1;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author asus
 */
public class View {
   JFrame form = new JFrame("FormMahasiswa");
   JPanel panel = new JPanel();
   JLabel nimLabel = new JLabel(" NIM : ");
   JLabel namaLabel = new JLabel(" Nama : ");
   JLabel umurLabel = new JLabel(" Umur : ");
   JLabel asalLabel = new JLabel(" Asal : ");
   JTextField nimTextField = new JTextField(6);
   JTextField namaTextField = new JTextField(24);
   JTextField umurTextField = new JTextField(2);
   JComboBox asalComboBox = new JComboBox(
           new String[] {"Jakarta","Bogor","Depok","Tangerang","Bekasi"}
   );
   JButton resetButton = new JButton("Reset");
   JButton simpanButton = new JButton("Simpan");
   
   public View(){
       form.setResizable (false);
       form.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       form.setVisible(true);
       Container pane = form.getContentPane();
       pane.setLayout(new GridBagLayout());
       GridBagConstraints c = new GridBagConstraints();
       
       c.fill = GridBagConstraints.HORIZONTAL;
       c.gridx =0;
       c.gridy = 0;
       pane.add(nimLabel, c);
       
       c.fill = GridBagConstraints.HORIZONTAL;
       c.gridx = 1;
       c.gridy = 0;
       c.insets = new Insets(0,150,0,75);
       pane.add(nimTextField, c);
       
       c.fill = GridBagConstraints.HORIZONTAL;
       c.gridx =0;
       c.gridy = 1;
       c.insets = new Insets(0,0,0,0);
       pane.add(namaLabel, c);
       
       c.fill = GridBagConstraints.HORIZONTAL;
       c.gridx = 1;
       c.gridy = 1;
       c.insets = new Insets(0,75,0,75);
       pane.add(namaTextField, c); 
       
       c.fill = GridBagConstraints.HORIZONTAL;
       c.gridx =0;
       c.gridy = 2;
       c.insets = new Insets(0,0,0,0);
       pane.add(umurLabel, c);
       
       c.fill = GridBagConstraints.HORIZONTAL;
       c.gridx = 1;
       c.gridy = 2;
       c.insets = new Insets(0,225,0,75);
       pane.add(umurTextField, c); 
       
       c.fill = GridBagConstraints.HORIZONTAL;
       c.gridx =0;
       c.gridy = 3;
       c.insets = new Insets(0,0,0,0);
       pane.add(asalLabel, c);
       
       c.fill = GridBagConstraints.HORIZONTAL;
       c.gridx = 1;
       c.gridy = 3;
       c.insets = new Insets(0,100,0,75);
       pane.add(asalComboBox, c); 
       
       c.fill = GridBagConstraints.HORIZONTAL;
       c.gridx =0;
       c.gridy = 4;
       c.ipadx = 175;
       c.insets = new Insets(20,5,5,0);
       pane.add(resetButton, c);
       
       c.fill = GridBagConstraints.HORIZONTAL;
       c.gridx = 1;
       c.gridy = 4;
       c.gridwidth = 2;
       c.insets = new Insets(20,0,5,75);
       pane.add(simpanButton, c);
       
       form.pack();
       
   }

    public JFrame getForm() {
        return form;
    }

    public void setForm(JFrame form) {
        this.form = form;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public JLabel getNimLabel() {
        return nimLabel;
    }

    public void setNimLabel(JLabel nimLabel) {
        this.nimLabel = nimLabel;
    }

    public JLabel getNamaLabel() {
        return namaLabel;
    }

    public void setNamaLabel(JLabel namaLabel) {
        this.namaLabel = namaLabel;
    }

    public JLabel getUmurLabel() {
        return umurLabel;
    }

    public void setUmurLabel(JLabel umurLabel) {
        this.umurLabel = umurLabel;
    }

    public JLabel getAsalLabel() {
        return asalLabel;
    }

    public void setAsalLabel(JLabel asalLabel) {
        this.asalLabel = asalLabel;
    }

    public JTextField getNimTextField() {
        return nimTextField;
    }

    public void setNimTextField(JTextField nimTextField) {
        this.nimTextField = nimTextField;
    }

    public JTextField getNamaTextField() {
        return namaTextField;
    }

    public void setNamaTextField(JTextField namaTextField) {
        this.namaTextField = namaTextField;
    }

    public JTextField getUmurTextField() {
        return umurTextField;
    }

    public void setUmurTextField(JTextField umurTextField) {
        this.umurTextField = umurTextField;
    }

    public JComboBox getAsalComboBox() {
        return asalComboBox;
    }

    public void setAsalComboBox(JComboBox asalComboBox) {
        this.asalComboBox = asalComboBox;
    }

    public JButton getResetButton() {
        return resetButton;
    }

    public void setResetButton(JButton resetButton) {
        this.resetButton = resetButton;
    }

    public JButton getSimpanButton() {
        return simpanButton;
    }

    public void setSimpanButton(JButton simpanButton) {
        this.simpanButton = simpanButton;
    }
}