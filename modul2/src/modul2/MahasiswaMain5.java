/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 *
 * Author = 222112090_Hanun Nabila Azis
 * Deskripsi singkat kelas
 * kelas yang memanggil konstruktor di kelas Mahasiswa4
 */
public class MahasiswaMain5 {//inisialisasi kelas
    public static void main(String args []) {
        Mahasiswa4 s1 = new Mahasiswa4 (222112090, "Hanun");
        Mahasiswa4 s2 = new Mahasiswa4 (222112100, "Nabila");
        
        s1.tampilkanInfo(); //tampil data
        s2.tampilkanInfo();
    }
}
