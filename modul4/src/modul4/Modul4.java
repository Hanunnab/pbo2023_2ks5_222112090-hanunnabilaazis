/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package modul4;

import java.util.Date;

/**
 *
 * @author hanunnab
 */
public class Modul4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //membuat objek dari abstrat Orang
        /*Orang o = new Orang();*/  //abstract hanya bisa untuk referensi, tidak untuk membuat objek baru
        Pegawai lutfi = new Pegawai(); //dapat membuat objek baru karena pegawai bukan abstract
        
        //memberikan dan mengambil nilai pada alamat melalui metode getter setter alamat
        lutfi.setAlamat ("Otista 64 C");
        System.out.println(lutfi.getAlamat());
        
        lutfi.setNIDN("12345678");
        lutfi.setKelompokKeahlian("Computer Science");
        
        System.out.println("ada dosen lutfi dengan NIDN"+lutfi.getNIDN()+" kelompok "+lutfi.getKelompokKeahlian());
    }
    
}
