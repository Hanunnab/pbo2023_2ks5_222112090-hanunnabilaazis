/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul3;
import java.util.Date;

/**
 *
 * Author : Hanun Nabila Azis
 */
public class Orang {
    private String nama;
    private Date tanggalLahir;
    
    public Orang () {
    }
    
    public Orang (String nama) {
        this.nama = nama;
    }
    
    public Orang (String nama, Date tanggalLahir) {
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }
    
    public void setNama (String nama) {
        this.nama = nama;
    }
    
    public String getNama () {
        return nama;
    }
    
    public Date getTanggalLahir () {
        return tanggalLahir;
    }
    
    public void setTanggalLahir (Date tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }
    
    public String getGaji () {
        return "tidak ada";
    }
}
