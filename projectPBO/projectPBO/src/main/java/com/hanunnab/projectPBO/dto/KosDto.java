/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.projectPBO.dto;

import com.hanunnab.projectPBO.entity.Daerah;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author asus
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KosDto {
    private Long id;
    @NotEmpty (message = "nama kos tidak boleh kosong")
    private String namaKos;
    @NotEmpty (message = "fasilitas tidak boleh kosong")
    private String fasilitas;
    @NotEmpty (message = "harga kos tidak boleh kosong")
    private String harga;
    @NotEmpty (message = "kontak pemilik/penjaga kos tidak boleh kosong")
    private String kontak;
    
    //@ManyToOne (cascade = CascadeType.ALL,fetch = FetchType.LAZY) 
    private Daerah daerah;
    
    @Override
    public String toString() {
        return namaKos;
    }
}
