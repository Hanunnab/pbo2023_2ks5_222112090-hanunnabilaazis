/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.studentdemocrudwebapp.mapper;

/*class sebagai mapper antara objek Student dan StudentDto,
untuk mengkonversi antara kelas entity dan kelas Dto*/

import com.hanunnab.studentdemocrudwebapp.dto.StudentDto; 
import com.hanunnab.studentdemocrudwebapp.entity.Student;
/**
 *
 * @author asus
 */
public class StudentMapper {
    //map Student entity to Student Dto
    public static StudentDto mapToStudentDto (Student student) {
        //membuat dto dengan builder pattern (inject dari lombok)
        StudentDto studentDto = StudentDto.builder()
            .id(student.getId())
            .firstName (student.getFirstName() )
            .lastName (student.getLastName() )
            .birthDate (student.getBirthDate() )
            .createdOn (student.getCreatedOn() )
            .updatedOn(student.getUpdatedOn() )
            .build();
return studentDto;
    }
    public static Student mapToStudent (StudentDto studentDto) {
    Student student = Student.builder()
        .id (studentDto.getId())
        .firstName (studentDto. getFirstName() )
        .lastName (studentDto.getLastName() )
        .birthDate (studentDto.getBirthDate())
        .createdOn (studentDto.getCreatedOn() )
        .updatedOn (studentDto.getUpdatedOn())
        .build();
    return student;
    }
}
