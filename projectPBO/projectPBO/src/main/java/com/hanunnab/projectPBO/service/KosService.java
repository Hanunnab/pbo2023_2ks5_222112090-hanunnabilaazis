/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.hanunnab.projectPBO.service;

import com.hanunnab.projectPBO.dto.KosDto;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author asus
 */
public interface KosService {
    public Page<KosDto> ambilKos(Pageable pageable);
    public void perbaruiDataKos(KosDto kosDto);
    public void hapusDataKos(Long kosid);
    public void simpanDatakos(KosDto kosDto);
    public KosDto cariById(Long id);
    
    public Page<KosDto> searchKos (String keyword, Pageable pageable);
}
