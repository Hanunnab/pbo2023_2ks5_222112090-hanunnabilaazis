/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.hanunnab.projectPBO.repository;

import com.hanunnab.projectPBO.dto.KosDto;
import com.hanunnab.projectPBO.entity.Kos;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author asus
 */
@Repository
public interface KosRepository extends JpaRepository<Kos, Long> {
    @Query("SELECT new com.hanunnab.projectPBO.dto.KosDto(k.id, k.namaKos, k.fasilitas, k.harga, k.kontak, k.daerah) FROM Kos k WHERE k.fasilitas LIKE %:keyword%")
    Page<KosDto> findByFasilitasContaining(@Param("keyword") String keyword, Pageable pageable);

    //Page<KosDto> findByFasilitasContaining (String keyword, Pageable pageable);
    
}
