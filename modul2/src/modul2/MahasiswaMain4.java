/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 *
 * Author = 222112090_Hanun Nabila Azis
 * Deskripsi singkat kelas
 * kelas dengan method utama di mana atributnya memanggil dari kelas Mahasiswa3
 * 
 */
public class MahasiswaMain4 {
    public static void main(String args[]) {
        Mahasiswa3 s1 = new Mahasiswa3 ();
        Mahasiswa3 s2 = new Mahasiswa3 ();
        
        s1.tambahData(222112090,"Hanun");
        s2.tambahData(222112100,"Nabila");
        s1.tampilkanInfo();
        s2.tampilkanInfo();
    }
}
