/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.projectPBO.service;

import com.hanunnab.projectPBO.repository.DaerahRepository;
import org.springframework.stereotype.Service;
import com.hanunnab.projectPBO.entity.Daerah;
import com.hanunnab.projectPBO.dto.DaerahDto;
import com.hanunnab.projectPBO.mapper.DaerahMapper;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author asus
 */
@Service
public class DaerahServiceImpl implements DaerahService {
    private DaerahRepository daerahRepository;
    public DaerahServiceImpl (DaerahRepository daerahRepository) {
        this.daerahRepository = daerahRepository;
    }
    
    @Override
    public List<DaerahDto> ambilDaerah() {
        List<Daerah> daerahs = this.daerahRepository.findAll();
        List<DaerahDto> daerahDtos = daerahs.stream()
                .map(daerah -> (DaerahMapper.mapToDaerahDto(daerah)))
                .collect(Collectors.toList());
        return daerahDtos;
    }
    
    @Override
    public void hapusDataDaerah (Long daerahId) {
        this.daerahRepository.deleteById(daerahId);
    }
    
    @Override
    public void perbaruiDataDaerah (DaerahDto daerahDto) {
        Daerah daerah = DaerahMapper.mapToDaerah(daerahDto);
        this.daerahRepository.save(daerah);
    }
    
    @Override
    public void simpanDataDaerah (DaerahDto daerahDto) {
        Daerah daerah = DaerahMapper.mapToDaerah(daerahDto);
        daerahRepository.save(daerah);
    }
    
    @Override
    public DaerahDto cariById(Long id) {
        Daerah daerah = daerahRepository.findById(id).orElse(null);
        DaerahDto daerahDto = DaerahMapper.mapToDaerahDto(daerah);
        return daerahDto;
    }

}

