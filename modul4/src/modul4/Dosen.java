/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul4;

/**
 *
 * @author hanunnab
 */
interface Dosen { //interface dosen
    //method tanpa body yang dapat diimplementasikan oleh kelas lain
    public String getNIDN(); //metode getter untuk mengambil nilai NIDN
    public void setNIDN(String NIDN); //metode setter untuk mengeset nilai NIDN
    public String getKelompokKeahlian(); //metode getter untuk mengambil nilai KelompokKeahlian
    public void setKelompokKeahlian(String kelompokKeahlian);//metode setter untuk mengeset nilai KelompokKeahlian
}
