/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.hanunnab.projectPBO.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.hanunnab.projectPBO.entity.User;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
/**
 *
 * @author asus
 */
public interface UserRepository extends JpaRepository<User, Long>{
    User findByEmail(String email);
}
