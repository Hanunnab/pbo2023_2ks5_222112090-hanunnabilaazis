/*

 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.projectPBO.controller;

import com.hanunnab.projectPBO.dto.DaerahDto;
import java.util.List;
import com.hanunnab.projectPBO.service.KosService;
import com.hanunnab.projectPBO.repository.KosRepository;
import org.springframework.stereotype.Service;
import com.hanunnab.projectPBO.entity.Kos;
import com.hanunnab.projectPBO.dto.KosDto;
import com.hanunnab.projectPBO.entity.Daerah;
import com.hanunnab.projectPBO.mapper.KosMapper;
import com.hanunnab.projectPBO.repository.DaerahRepository;
import jakarta.validation.Valid;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author asus
 */
@Controller
public class KosController {
    private KosService kosService;
    public KosController(KosService kosService){
        this.kosService = kosService;
    }
    @Autowired
    private DaerahRepository daerahRepo;
    
    //handler method, GET Request return model (dto) dan View (templates/*.html)
@GetMapping("/admin/koss")
public String koss(Model model, @RequestParam(defaultValue = "") String keyword,
                   @RequestParam(defaultValue = "10") int pageSize,
                   @RequestParam(defaultValue = "1") int pageNo) {
    model.addAttribute("keyword", keyword);

    Pageable pageable = PageRequest.of(pageNo - 1, pageSize);

    Page<KosDto> kosPage;
    if (keyword.isEmpty()) {
        kosPage = kosService.ambilKos(pageable);
    } else {
        kosPage = kosService.searchKos(keyword, pageable);
    }
    
    model.addAttribute("kosDtos", kosPage.getContent());
    model.addAttribute("currentPage", pageNo);
    model.addAttribute("totalPages", kosPage.getTotalPages());
    model.addAttribute("notFound", kosPage.isEmpty());

    List<Integer> pageSizes = Arrays.asList(5, 10, 20); // Define available page sizes
    model.addAttribute("pageSizes", pageSizes);

    return "admin/koss"; // Make sure the template path is correct
}

    
    @GetMapping("/admin/koss/add")
    public String addKosForm(Model model){
        KosDto kosDto = new KosDto();
        List<Daerah> listDaerahs = daerahRepo.findAll();
        model.addAttribute("listDaerahs", listDaerahs);
        // tambah atribut "studentDto" yang bisa/akan digunakan di form th:object
        model.addAttribute("kosDto",kosDto );
        // thymeleaf view: "/templates/admin/students.html"
        return "/admin/kos_add_form";
    }
    
    @GetMapping("/")
    public String index (){
        return "index";
    }
    
    @PostMapping("/admin/koss/add")
    public String addKos(@Valid KosDto kosDto, BindingResult result,Model model){
        if (result.hasErrors()){
            List<Daerah> listDaerahs = daerahRepo.findAll();
            model.addAttribute("listDaerahs", listDaerahs);
            // model.addAttribute("studentDto", studentDto);
            return "/admin/kos_add_form";
        }
        kosService.simpanDatakos(kosDto);
        return "redirect:/admin/koss";
    }
    @GetMapping("/admin/koss/delete/{id}")
    public String deleteKos(@PathVariable("id") Long id){
        kosService.hapusDataKos(id); 
        return "redirect:/admin/koss";
    }
    @GetMapping("/admin/koss/update/{id}")
    public String updateKosForm(@PathVariable("id") Long id, Model model){
        KosDto kosDto = kosService.cariById(id); 
        List<Daerah> listDaerahs = daerahRepo.findAll();
        model.addAttribute("listDaerahs", listDaerahs);
        model.addAttribute("kosDto", kosDto);
        return "/admin/kos_update_form";
    }


    @PostMapping("/admin/koss/update")
    public String updateKos(@Valid @ModelAttribute("koss") KosDto kosDto, BindingResult result){
        if (result.hasErrors()){
            // model.addAttribute("studentDto", studentDto);
            return "/admin/kos_update_form";
        }
        kosService.perbaruiDataKos(kosDto);
        return "redirect:/admin/koss";
    }
}