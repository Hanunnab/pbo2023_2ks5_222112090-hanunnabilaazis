/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.hanunnab.studentdemocrudwebapp.repository;

/*interface untuk mengakses data dari database untuk entitas Student 
menggunakan Spring Data JPA.*/
import com.hanunnab.studentdemocrudwebapp.entity.Student;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
        
/**
 *
 * @author asus
 */
public interface StudentRepository extends JpaRepository<Student, Long> {
    //contoh method abstract baru
    Optional<Student> findByLastName(String lastName);
}
