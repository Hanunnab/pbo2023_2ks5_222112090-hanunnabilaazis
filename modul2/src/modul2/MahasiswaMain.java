/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modul2;

/**
 *
 * Author = 222112090_Hanun Nabila Azis
 * Deskripsi singkat kelas
 * berisikan main program yang memamnggil atribut (reference) dari kelas lain
 */
public class MahasiswaMain { //membuat kelas MahasiswaMain
    public static void main(String args[]){
        Mahasiswa2 s1 = new Mahasiswa2(); //membuat objek
        System.out.println(s1.nim); //memanggil variabel referensi mahasiswas1 dari kelas Mahasiswa2
        System.out.println(s1.nama);
    }
}
