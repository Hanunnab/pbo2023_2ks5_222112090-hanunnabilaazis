/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package latihangui1;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asus
 */
public class Model {
    private String nim;
    private String nama;
    private int umur;
    private String asal;
    private List pesanErrors = new ArrayList();
     private List daftarMahasiswa = new ArrayList();
    
    public Model(String nim, String nama, int umur, String asal) {
        this.nim = nim;
        this.nama = nama;
        this.umur = umur;
        this.asal = asal;
    }
    
    public Model(){
        
    }
    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur() {
        return umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public List getPesanError() {
        return pesanErrors;
    }

    public void setPesanError(List pesanError) {
        this.pesanErrors = pesanError;
    }

    
    //pesan error
    public void addPesanError(String pesanError){
        pesanErrors.add(pesanError);
    }
    
    public List getPesanErrors(){
        return pesanErrors;
    }
    
    public void hapusPesanErrors(){
        pesanErrors.clear();
    }
    
    public void addMahasiswa(String mahasiswa) {
        daftarMahasiswa.add(mahasiswa);
    }

    public List getDaftarMahasiswa() {
        return daftarMahasiswa;
    }

    @Override
    public String toString() {
        return nim + " - " + nama ;
    }
}
