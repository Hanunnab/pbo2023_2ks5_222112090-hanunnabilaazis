/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.projectPBO.service;
import com.hanunnab.projectPBO.repository.KosRepository;
import org.springframework.stereotype.Service;
import com.hanunnab.projectPBO.entity.Kos;
import com.hanunnab.projectPBO.dto.KosDto;
import com.hanunnab.projectPBO.mapper.KosMapper;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
/**
 *
 * @author asus
 */
@Service
public class KosServiceImpl implements KosService{
    private KosRepository kosRepository;
    public KosServiceImpl(KosRepository kosRepository) {
        this.kosRepository = kosRepository;
    }
    
    @Override
    public Page<KosDto> ambilKos(Pageable pageable) {
        return kosRepository.findAll(pageable)
                .map(KosMapper::mapToKosDto);
    }
    
    @Override
    public void hapusDataKos (Long kosId) {
        this.kosRepository.deleteById(kosId);
    }
    
    @Override
    public void perbaruiDataKos (KosDto kosDto) {
        Kos kos = KosMapper.mapToKos(kosDto);
        this.kosRepository.save(kos);
    }
    
    @Override
    public KosDto cariById(Long id) {
        Kos kos = kosRepository.findById(id).orElse(null);
        KosDto kosDto = KosMapper.mapToKosDto(kos);
        return kosDto;
    }

    @Override
    public void simpanDatakos(KosDto kosDto) {
        Kos kos = KosMapper.mapToKos(kosDto);
        kosRepository.save(kos);  
    } 
    
    @Override
public Page<KosDto> searchKos(String keyword, Pageable pageable) {
    return kosRepository.findByFasilitasContaining(keyword, pageable);
}
}
