/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package latihangui1;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author asus
 */
public class Controller {
    private View view;
    private Model model;

    public Controller(Model model, View view) {
        this.view = view;
        this.model = model;
    }
    public Controller(){
        
    }

    public void simpanModel(){
        String data[] = new String[4];
        
        data[0] = view.getNimTextField().getText();
        data[1] = view.getNamaTextField().getText();
        data[2] = view.getUmurTextField().getText();
        data[3] = view.getAsalComboBox().getSelectedItem().toString();
        
        //verifikasi
        if(data[0].length() == 6){
            model.setNim(data[0]);
        }else if(data[0].length() == 0){
            model.addPesanError("Nim harus diisi");
        }else{
            model.addPesanError("Nim harus 6 digit");
        }
        
        if(data[1].length() > 0 ){
            if(data[1].length() <50){
                model.setNama(data[1]);
            }else{
                model.addPesanError("Nama maksimal 50 digit");
            }
        }else{
            model.addPesanError("Nama harus diisi");
        }
        
        if(data[2].length() >0){
            try{
                int umur = Integer.parseInt(data[2]);
                model.setUmur(umur);
            }catch (NumberFormatException e){
                model.addPesanError("Umur harus berupa angka");
            }
        }else{
            model.addPesanError("Umur harus diisi");
        }
        
        model.setAsal((view.getAsalComboBox().getSelectedItem().toString()));
        
        List<String> errors = model.getPesanErrors();
        if(model.getPesanErrors().size() >0){
            StringBuilder sb = new StringBuilder();
            for(String pesanErrors : errors){
                sb.append(pesanErrors).append("\n");
            }
            JOptionPane.showMessageDialog(null, sb.toString(), "Terjadi kesalahan input pada : ",
                    JOptionPane.ERROR_MESSAGE);
            sb = null;
            model.hapusPesanErrors();
        }else{
            model.addMahasiswa(model.toString());
            List<String> daftarMahasiswa = model.getDaftarMahasiswa();
            StringBuilder sb = new StringBuilder();
            for(String mahasiswa : daftarMahasiswa){
                sb.append(mahasiswa).append("\n");
            }
            JOptionPane.showMessageDialog(null, sb.toString(),"Daftar NIM dan Nama Mahasiswa yang telah berhasil disimpan : ",JOptionPane.INFORMATION_MESSAGE);
        }
    }
       
    public void reset() {
        view.getNimTextField().setText(null);
        view.getNamaTextField().setText(null);
        view.getUmurTextField().setText(null);
        view.getAsalComboBox().setSelectedIndex(0);
    }
    
    public void run(){
        this.model.hapusPesanErrors();
        view.getSimpanButton().addActionListener(e->simpanModel());
        view.getResetButton().addActionListener(e->reset());
    }
}
