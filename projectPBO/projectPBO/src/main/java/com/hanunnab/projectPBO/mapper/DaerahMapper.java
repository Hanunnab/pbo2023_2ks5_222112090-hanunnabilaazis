/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hanunnab.projectPBO.mapper;

import com.hanunnab.projectPBO.dto.DaerahDto;
import com.hanunnab.projectPBO.entity.Daerah;

/**
 *
 * @author asus
 */
public class DaerahMapper {
    //map Student entity to Student Dto
    public static DaerahDto mapToDaerahDto (Daerah daerah) {
        //membuat dto dengan builder pattern (inject dari lombok)
        DaerahDto daerahDto = DaerahDto.builder()
            .id (daerah.getId())
            .namaDaerah (daerah.getNamaDaerah() )
            .kos(daerah.getKos())
            .build();
return daerahDto;
    }
    public static Daerah mapToDaerah (DaerahDto daerahDto) {
    Daerah daerah = Daerah.builder()
        .id (daerahDto.getId())
        .namaDaerah (daerahDto.getNamaDaerah())
        .kos(daerahDto.getKos())
        .build();
    return daerah;
    }
}
