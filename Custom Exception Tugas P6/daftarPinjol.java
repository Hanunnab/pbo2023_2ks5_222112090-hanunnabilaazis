/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/*
author @hanunnab
Program daftarPinjol yang mana user diminta untuk menginput nama dan umur.
apabila user diatas 19 tahun, maka user diperbolehkan untuk mengajukan pinjaman.
terdapat exception apabila umur yang diinput dibawah 0 tahun.
*/
package tugas.custom.exception;

import java.util.Scanner; //mengimpor scanner untuk input nama dan umur

public class daftarPinjol { //membuat kelas daftarPinjol
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in); //objek scanner untuk membaca input pengguna

        System.out.print("Berikan Identitas Anda\n");
        System.out.print("Nama  : "); //input nama pengguna
        String name = input.nextLine(); //membaca inputan pengguna dengan method nextLine dari objek scanner

        try {
            System.out.print("Umur  : ");
            int age = input.nextInt(); //mencetak inputan umur

            if (age >= 19) {
                System.out.println("Saudara " + name + " diperbolehkan untuk mengajukan pinjaman!");
            } else if (age >= 0 && age < 19) {
                System.out.println("Mohon maaf " + name + ", Anda masih di bawah umur, sebaiknya minta orangtua saja.");
            } else {
                throw new IllegalArgumentException("Umur tidak valid");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}