/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.hanunnab.projectPBO.service;

import com.hanunnab.projectPBO.dto.DaerahDto;
import java.util.List;

/**
 *
 * @author asus
 */
public interface DaerahService {
    public List<DaerahDto> ambilDaerah();
    public void perbaruiDataDaerah(DaerahDto daerahDto);
    public void hapusDataDaerah(Long daerahid);
    public void simpanDataDaerah(DaerahDto daerahDto);
    public DaerahDto cariById(Long id);
}
